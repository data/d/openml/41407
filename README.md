# OpenML dataset: sf2

https://www.openml.org/d/41407

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multivariate regression data set from: https://link.springer.com/article/10.1007%2Fs10994-016-5546-z : The Solar Flare dataset (Lichman 2013) has 3 target variables that correspond to the number of times 3 types of solar flare (common, moderate, severe) are observed within 24 h. There are two versions of this dataset. SF1 contains data from year 1969 and SF2 from year 1978.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41407) of an [OpenML dataset](https://www.openml.org/d/41407). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41407/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41407/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41407/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

